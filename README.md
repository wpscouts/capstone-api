# Mongoose-API

An API backend based on express.js (framework) + mongoDB (database). It supports user authetication based on (Passport.js + JWT). It also includes payment processor (i.e. Stipe) based on subscription based model.

This package contains POSTMAN collection file, it is located in `./postman`. This file can be imported in POSTMAN and API routes can be tested.

## Routes

	POST /users/register // (creates a new record in 'users' collection)
	GET /users/profile // (returns logged-in user info)
	PUT /users/update // (updates logged-in user info)
	DELETE /users/delete // (delete logged-in user and all linked records)

	POST /auth/login // (autheticates user credentials and creates a token)

	GET /settings // (returns logged-in user settings)
	PUT /settings // (updates logged-in user settings)

	GET /pages // (get a list of all pages of logged-in user)
	POST /pages // (creates a new record in 'pages' collection)
	GET /pages/:id // (returns page document with the given ID)
	PUT /pages/:id // (update a page document with given ID)
	DELETE /pages/:id // (deletes a page document with given ID)

	GET /themes // (returns a list of all (themes + info) in ./themes folder)
	GET /themes/:name // (returns information about a given theme)
	GET /themes/:name/activate // (activates the theme with given name)

	POST /billing/token // (creates a one-time use stripe token for following actions)

	POST /billing/subscription // (creates a customer + subscribe the logged-in user)
	GET /billing/subscription // (returns subscription info of logged-in user)
	PUT /billing/subscription // (updates subscription info of logged-in user)
	DELETE /billing/subscription // (delete subscription info of logged-in user)


## Authetication

API's authetication is based on passport.js + JSON Web Token (JWT).

1. To autheticate a user, send username and password to `POST /auth/login` route.
2. If user is autheticated it will return a `token` string as an access token.
3. Store this string on your client-side and send this token on each subsequent request to access any protected route.
4. To send this token for the access, use `Authorization` header with a value of `Bearer ACCESS_TOKEN`

## Payment Processor

API's payment processing features is subscription based.

1. Before creating a subscription, the user must have a registered account with this app.
2. If user is registered, it'll need to access `POST /billing/token` route to create a one-time use stripe token, this token will be used in subsequent mutable requests to the stripe sever.
3. Each mutable action (i.e. POST, PUT, DELETE) on `/billing/subscription` resource will not only change user's subscription details on our database but it'll also change user's information on stripe server.
4. A registered `user` of our app will not be regarded as a `customer` if he/she has not subscribed to any paid plan of our app, in any point of time. However, if the user is also a customer he/she can unsubscibe later in time and his/her customer's record would not be deleted from our DB and stripe server. Although, on unsubscription, the customer's subscription data would be deleted from both places.

The more detailed documentation on each route can be found in POSTMAN package file.
// BASE SETUP
// =============================================================================

// Import Modules
import _ from 'lodash';
import express from 'express';
import mongoose from 'mongoose';
import cors from 'cors';
import path from 'path';
import dotenv from 'dotenv';
import bodyParser from 'body-parser';
import helmet from 'helmet';
import expressValidator from 'express-validator';

// import environmental variables
dotenv.config();

// Import Routes
import apiRoutes from './routes.js';

// Import Helpers
import errorHandlers from './shared/errors/errors-handler';


// create our Express app
const app = express();

// view engine setup
app.set('views', path.join(__dirname, './themes')); // this is the folder where we keep our pug files
app.set('view engine', 'pug'); // we use the engine pug, mustache or EJS work great too



// CONFIGURE MIDDLE-WARES (Application Level)
// =============================================================================

// Takes the raw requests and turns them into usable properties on req.body
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Exposes a bunch of methods for validating data.
app.use(expressValidator());

// secure apps by setting various HTTP headers
app.use(helmet());

// allow cross-origin resource sharing
app.use(cors());

// map req.body.payload => req.body
// postman will send data in req.body
// vue.js will send data in req.body.payload
app.use(function (req, res, next) {
	const hasPayload = _.has(req.body, 'payload');
	if (hasPayload) {
		req.body = req.body.payload;
	}
	next()
})

// Exposes folder at `./src/public` for serving directly
app.use('/public', express.static(path.join(__dirname, '/public')));



// REGISTER ROUTES
// =============================================================================

app.use('/api/v1', apiRoutes); // routes for our API


// CONFIGURE MIDDLE-WARES (Application Level) - Error Handlers
// =============================================================================


// If that above routes didnt work, we 404 them and forward to error handler
app.use(errorHandlers.notFound);

// Otherwise this was a really bad error we didn't expect! Shoot eh
if (app.get('env') === 'development') {
  /* Development Error Handler - Prints stack trace */
  app.use(errorHandlers.developmentErrors);
}

// production error handler
app.use(errorHandlers.productionErrors);




// CONNECT DATABASE
// =============================================================================

// connection attempt
const mongoOptions = { useMongoClient: true, promiseLibrary: global.Promise }
mongoose.connect(process.env.DATABASE, mongoOptions);

// connection events
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
	console.error('We are connected.'); // we're connected!
});



// START THE SERVER
// =============================================================================

app.set('port', process.env.PORT || 8000); // set our port

const server = app.listen(app.get('port'), () => {
	console.log(`Express running → PORT ${server.address().port}`);
});

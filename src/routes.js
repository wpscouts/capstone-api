// ROUTES
// =============================================================================

// Import Modules
import express from 'express';

// Import Controller(s)
import * as pages from './components/pages/pages-controller';
import * as users from './components/users/users-controller';
import * as settings from './components/settings/settings-controller';
import * as themes from './components/settings/themes-controller';
import * as customers from './components/customers/customers-controller';
import * as subscriptions from './components/customers/subscriptions-controller';
import * as cards from './components/customers/cards-controller';
import * as authorization from './components/authorization/authorization-controller';
import * as authentication from './components/authentication/authentication-controller';


// Import Handlers
// import * as ehl from '../../shared/errors/errors-handler';

// Import Middlewares
import * as amw from './components/authentication/authentication-middleware';
import * as pmw from './components/pages/pages-middleware';
import * as umw from './components/users/users-middleware';
import * as smw from './components/settings/settings-middleware';

// get an instance of the express Router
const router = express.Router(); 

// routes defintion
router.get('/', (req, res) => {
  res.json({ message: 'This is admin dashboard.' });
});

router.post('/users/register', umw.isvalidRegister, users.create);
router.get('/users/profile', amw.checkToken, users.read);
router.put('/users/update', amw.checkToken, amw.checkSubscription, umw.validateNewPassword, umw.checkOriginalPassword, users.update);
router.delete('/users/delete', amw.checkToken, amw.checkSubscription, users.destroy);

router.post('/authentication/token', amw.checkToken, authentication.token);
router.post('/authentication/login', authentication.create);

router.get('/settings', amw.checkToken, amw.checkSubscription, settings.read);
router.put('/settings', amw.checkToken, amw.checkSubscription, smw.isvalidSubdomain, settings.update);

router.get('/settings/themes', amw.checkToken, amw.checkSubscription, smw.getThemes, themes.index);
router.put('/settings/themes', amw.checkToken, amw.checkSubscription, smw.getThemes, themes.update);
router.get('/settings/themes/:name', amw.checkDomain, amw.checkSubscription, smw.getThemes, themes.read);


router.get('/authorization', amw.checkToken, amw.checkSubscription, authorization.read);
router.put('/authorization', amw.checkToken, amw.checkSubscription, authorization.update);

router.get('/pages', amw.checkToken, amw.checkSubscription, pages.index);
router.post('/pages', amw.checkToken, amw.checkSubscription, pmw.isAuthorized, pages.create);
router.get('/pages/:slug', amw.checkToken, amw.checkSubscription, pages.read);
router.put('/pages/:slug', amw.checkToken, amw.checkSubscription, pages.update);
router.delete('/pages/:slug', amw.checkToken, amw.checkSubscription, pages.destroy);

router.post('/customers/token', amw.checkToken, customers.token);

router.post('/customers/subscription', amw.checkToken, subscriptions.create);
router.get('/customers/subscription', amw.checkToken, subscriptions.read);
router.put('/customers/subscription', amw.checkToken, subscriptions.update);
router.delete('/customers/subscription', amw.checkToken, subscriptions.destroy);

router.post('/customers/card', amw.checkToken, cards.create);
router.get('/customers/card', amw.checkToken, cards.read);
router.delete('/customers/card', amw.checkToken, cards.destroy);




// export routes (used by main.js)
export default router;
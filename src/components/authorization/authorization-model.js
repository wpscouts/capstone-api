// AUTHORIZATION MODEL
// =============================================================================

// Import Modules
import mongoose from 'mongoose';
import validator from 'validator';

// Fix Deprecated Warning
mongoose.Promise = global.Promise;


// Define Schema
const AuthorizationSchema = new mongoose.Schema({
	user_id: {
		type: mongoose.Schema.ObjectId,
		ref: 'User',
		unique: true,
		required: 'You must attach this constraint profile with some user.'
	},
	page_limit: {
		type: Number,
		default: 3,
		required: 'Please provide a page limit for this user.'
	},
	portfolio_limit: {
		type: Number,
		default: 3,
		required: 'Please provide a portfolio limit for this user.'
	},
	media_limit: {
		type: Number,
		default: 10,
		required: 'Please provide a portfolio limit for this user.'
	},
	actions: {
		type: Array,
		default: ["twitter_tweet", "twitter_follow", "facebook_post", "facebook_like", "friend_refer"]
	}
});


// Export Schema
const AuthorizationModel = mongoose.model('Authorization', AuthorizationSchema);
export default AuthorizationModel;
// AUTHORIZATION CONTROLLER
// =============================================================================

// Import Modules
import _ from 'lodash';
import mongoose from 'mongoose';

// Import Models
import Authorization from '../authorization/authorization-model';

// Define Action(s)
export const read = async (req, res) => {
	const authorization = await Authorization.findOne({ user_id: req.user._id });
	res.status(200).json(authorization);
};


export const update = async (req, res) => {
	const authorization 	= await Authorization.findOne({ user_id: req.user._id });
	const allowed_actions 	= authorization.actions;
	const action_is_allowed = allowed_actions.includes(req.body.action);

	// check if the requested action is allowed or not
	if (!action_is_allowed) {
		return res.status(400).json({ message: 'You have already made this action.' });
	}

	// rewards against each action
	switch (req.body.action) {
		case 'facebook_like':
			var reward = { page_limit: 1, portfolio_limit: 1, media_limit: 5 };
			break; 
		case 'facebook_post':
			var reward = { page_limit: 1, portfolio_limit: 1, media_limit: 10 };
			break; 
		case 'twitter_follow':
			var reward = { page_limit: 1, portfolio_limit: 1, media_limit: 5 };
			break;
		case 'twitter_tweet':
			var reward = { page_limit: 1, portfolio_limit: 2, media_limit: 10 };
			break;
		case 'friend_refer':
			var reward = { page_limit: 1, portfolio_limit: 2, media_limit: 10 };
			break; 
		default: 
			var reward = { page_limit: 0, portfolio_limit: 0, media_limit: 0 };
	}

	// remove current action and increase limits
	const remaining_actions    = _.without(authorization.actions, req.body.action);
	const update_authorization = await authorization.set({ page_limit: (authorization.page_limit + reward.page_limit), portfolio_limit: (authorization.portfolio_limit + reward.portfolio_limit), media_limit: (authorization.media_limit + reward.media_limit), actions: remaining_actions }).save();
	
	res.status(200).json({ message: 'You\'ve successfully made this action.', data: update_authorization });
};

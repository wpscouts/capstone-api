// AUTHETICATION CONTROLLER
// =============================================================================

// Import Modules
import mongoose from 'mongoose';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';

// Import Models
import User from '../users/user-model';

/**
 * Authenticate JWT which is being sent in the request HEADER
 * @param  {object} req Request of the client coming in
 * @param  {object} res Response to the client from server
 * @return {object}     Returns JWT authentication status
 */
export const token = async (req, res) => {
	res.status(200).json({message: 'Token Authenticated.'});
};

/**
 * Authenticate username and password and generate JWT
 * @param  {object} req Request of the client coming in
 * @param  {object} res Response to the client from server
 * @return {object}     Returns JWT or 'message' stating error
 */
export const create = async (req, res) => {
	
	// check username against our record
	const user = await User.findOne({ email: req.body.email });

	// user not found
	if( !user ){
		return res.status(401).json({message: 'No such user found.'});
	}
	
	// check password against our record
	bcrypt.compare(req.body.password, user.password).then(function(isMatch) {
		if (isMatch) {
			// from now on we'll identify the user by the id and the id is the only personalized value that goes into our token
			const payload 	= {id: user.id};
			const secret 	= process.env.SECRET;
			const options 	= { /* expiresIn: 60 */ };

			const token 	= jwt.sign(payload, secret, options);
			return res.json({message: "User Authenticated.", token: token});
		} else {
			res.status(401).json({message: 'Passwords did not match.'});
		}
	});

};
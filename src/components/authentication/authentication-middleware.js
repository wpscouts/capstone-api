// AUTHENTICATION MIDDLEWARE
// =============================================================================

// Import Modules
import __ from 'lodash';
import mongoose from 'mongoose';
import jwt from 'jsonwebtoken';
import parseDomain from 'parse-domain';


// Import Models
import User from '../users/user-model';
import Page from '../pages/page-model';
import Setting from '../settings/setting-model';
import Customer from '../customers/customer-model';


/**
 * Check request domain/subdomain to identify user
 * If user is not identifiable respond with error message
 * 
 * @param  {object} req  Request of the client coming in
 * @param  {object} res  Response to the client from server
 * @param  {object} next Next middleware refrence
 * @return {object}     Returns JWT authentication status
 */
export const checkDomain = async (req, res, next) => {
	
	const hostname = parseDomain(req.hostname);
	const fullDomain = hostname.domain + '.' + hostname.tld;
	const isCustomDomain = (fullDomain == process.env.DOMAIN) ? false : true; 

	// if request has a subdomain
	if (hostname.subdomain) {

		// check if subdomain exists in our DB
		const setting 	= await Setting.findOne({ site_subdomain: hostname.subdomain });

		if (!setting) {
			return res.status(404).json({ message: "No such subdomain exist." });
		}

		// setup 'user' obj and attach user ID with it
		req.user = {}
		req.user._id = setting.user_id;

	} else {

		// otherwise, if is it a custom domain
		if (isCustomDomain) {

			// check if custom domain exists in our DB
			const setting 	= await Setting.findOne({ custom_url: fullDomain });

			if (!setting) {
				return res.status(404).json({ message: "No such custom domain exist." });
			}

			// setup 'user' obj and attach user ID with it
			req.user = {}
			req.user._id = setting.user_id;

		} else {

			return res.status(401).json({ message: "You can access this API only via your specific domain." });
		
		}

	}

	next(); // there were no errors!
};


/**
 * Authenticate JWT which is being sent in the request HEADER
 * 
 * @param  {object} req  Request of the client coming in
 * @param  {object} res  Response to the client from server
 * @param  {object} next Next middleware refrence
 * @return {object}      Returns JWT authentication status
 */
export const checkToken = async (req, res, next) => {

	// const domain_user = req.user._id;

	try {

		const token = req.header('Authorization');
		
		// Check if the token is even provided
		if (!token) {
			return res.status(401).json({ message: "The authetication token is not provided." });
		}

		// Verify if provided token is valid
		const payload = jwt.verify(token.replace('Bearer ',''), process.env.SECRET);
		const user = await User.findOne({ _id: payload.id });
		
		// Check if the token provided matches with the domain
		// const domain_token_match = __.isEqual(domain_user, user._id);
		// if (!domain_token_match) {
		// 	return res.status(404).json({ message: "Your domain or token is incorrect." });
		// }

		// Attach 'user' object with request and move on
		req.user = user;
		return next();

	} catch(err) {

		return res.status(401).json({ message: "The token is not authenticated." });

	}
};


/**
 * Check if the user has any valid subscription
 * Requires the 'user' object in request
 * 
 * @param  {object} req  Request of the client coming in
 * @param  {object} res  Response to the client from server
 * @param  {object} next Next middleware refrence
 * @return {object}      Returns subscription status if there is any error
 */

export const checkSubscription = async (req, res, next) => {

	const customer 	= await Customer.findOne({ user_id: req.user._id });

	// check if customer exists in our DB
	if (!customer) {
		return res.status(401).json({ message: "The user has no customer profile." });
	}

	// calculate customer subscripion validity
	const currentTimestamp 		= Math.floor(Date.now() / 1000);
	const subscriptionPeriodEnd = customer.subscription_period_end;
	const subscriptionValid 	= currentTimestamp <= subscriptionPeriodEnd;

	// if customer subscription has expired
	if (!subscriptionValid) {
		return res.status(401).json({ message: "Your subscription has expired." });
	}

	return next();

};




// API can ONLY be accessed via user subdomain or custom domain
// Protected routes will also check for authetication TOKEN
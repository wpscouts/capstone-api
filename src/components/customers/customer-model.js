// CUSTOMER MODEL
// =============================================================================

// Import Modules
import mongoose from 'mongoose';
import validator from 'validator';

// Fix Deprecated Warning
mongoose.Promise = global.Promise;


// Define Schema
const CustomerSchema = new mongoose.Schema({
	user_id: {
		type: mongoose.Schema.ObjectId,
		ref: 'User',
		unique: true,
		required: 'You must attach this customer profile with some user.'
	},
	stripe_customer_id: {
		type: String,
		unique: true,
		trim: true
	},
	stripe_subscription_id: {
		type: String,
		unique: true,
		trim: true
	},
	stripe_card_id: {
		type: String,
		trim: true
	},
	subscription_plan: { // used to decide customer level of access to dashboard
		type: String,
		unique: true,
		default: 'trial',
		trim: true
	},
	subscription_period_end: { // used to decide customer's access to dashboard
		type: String,
		unique: true,
		default: Math.floor(Date.now() / 1000) + (7 * 24 * 60 * 60),
		trim: true
	},

});


// Export Schema
const CustomerModel = mongoose.model('Customer', CustomerSchema);
export default CustomerModel;
// CUSTOMERS CONTROLLER
// =============================================================================

// Import Modules
import mongoose from 'mongoose';
import stripePackage from 'stripe';

//  Set Stripe Key
const stripe = stripePackage('sk_test_dZlZk75Gbz1XszV24M98eSwe');

// Import Models
import Customer from './customer-model';


// Define Action(s)
export const token = async (req, res) => {
	// it'll collect user card info and create a one-time use stripe token
	// Token should be create on the front-end because we don't want card number to hit our server
	// It'll include 3 steps: https://code.tutsplus.com/tutorials/how-to-accept-payments-with-stripe--pre-80957
	// Following is for testing purpose only
	const token = await stripe.tokens.create({ card: req.body });
	res.status(200).json({message: 'Token Created.', token: token});
};
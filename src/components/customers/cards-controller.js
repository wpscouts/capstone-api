// CARDS CONTROLLER
// =============================================================================

// Import Modules
import mongoose from 'mongoose';
import stripePackage from 'stripe';

//  Set Stripe Key
const stripe = stripePackage('sk_test_dZlZk75Gbz1XszV24M98eSwe');

// Import Models
import Customer from './customer-model';


// Define Action(s)


// create user subscription
export const create = async (req, res) => {
  const customer = await Customer.findOne({ user_id: req.user._id });

  // create (card) if "stripe card" does not exists
  if (!customer.stripe_card_id) {
    const stripe_card = await stripe.customers.createSource( customer.stripe_customer_id, { source: req.body.source } );
    const local_card  = await customer.set({ stripe_card_id: stripe_card.id }).save();
    return res.status(200).json({message: 'Card added successfully.'});
  }

  res.status(400).json({message: 'Customer has already a card added.'});

};

// read user card
export const read = async (req, res) => {
	const customer = await Customer.findOne({ user_id: req.user._id });

	if (!customer.stripe_card_id) {
		return res.status(400).json({message: 'Customer has not added any cards.'});
	}

	const stripe_cards = await stripe.customers.retrieveCard(customer.stripe_customer_id, customer.stripe_card_id);
	res.status(200).json({card: stripe_cards});
};

// delete user card
export const destroy = async (req, res) => {
  const customer = await Customer.findOne({ user_id: req.user._id });

  if (!customer.stripe_card_id) {
    return res.status(400).json({message: 'Customer has not added any cards.'});
  }

  const stripe_card = await stripe.customers.deleteCard(customer.stripe_customer_id, customer.stripe_card_id);
  const local_card  = await customer.set({ stripe_card_id: null }).save();
  res.status(200).json({message: 'Card removed successfully.'});
};

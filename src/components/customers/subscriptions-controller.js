// SUBSCRIPTIONS CONTROLLER
// =============================================================================

// Import Modules
import mongoose from 'mongoose';
import stripePackage from 'stripe';

//  Set Stripe Key
const stripe = stripePackage('sk_test_dZlZk75Gbz1XszV24M98eSwe');

// Import Models
import Customer from './customer-model';


// Define Action(s)

// create user subscription
export const create = async (req, res) => {
	const customer = await Customer.findOne({ user_id: req.user._id });

	// create (customer + subscription) if "stripe customer" do not exists
	if (!customer.stripe_customer_id) {
		const stripe_customer 		= await stripe.customers.create({ email: req.user.email, source: req.body.source });
		const stripe_subscription 	= await stripe.subscriptions.create({ customer: stripe_customer.id, items: [{ plan: req.body.plan }] });
		const local_customer 		= await customer.set({ user_id: req.user._id, stripe_customer_id: stripe_customer.id, stripe_subscription_id: stripe_subscription.id, stripe_card_id: stripe_customer.default_source, subscription_plan: stripe_subscription.plan.id, subscription_period_end: stripe_subscription.current_period_end }).save();
		return res.status(200).json({message: 'Customer created and subscribed successfully.', customer: customer});
	}

	// create (subscription) if "stripe subscription" it do not exists
	if (!customer.stripe_subscription_id) {
		const stripe_subscription = await stripe.subscriptions.create({ customer: customer.stripe_customer_id, items: [{ plan: req.body.plan }] });
		const local_subscription  = await customer.set({ stripe_subscription_id: stripe_subscription.id, subscription_plan: stripe_subscription.plan.id, subscription_period_end: stripe_subscription.current_period_end }).save();
		return res.status(200).json({message: 'Customer subscribed successfully.'});
	}

	res.status(400).json({message: 'Customer is already subscribed to a paid plan.'});

};

// read user subscription
export const read = async (req, res) => {
	const customer = await Customer.findOne({ user_id: req.user._id });

	if (!customer.stripe_subscription_id) {
		return res.status(400).json({message: 'Customer is not subscribed to any plan.'});
	}

	const stripe_subscription = await stripe.subscriptions.retrieve(customer.stripe_subscription_id);
	res.status(200).json({subscription: stripe_subscription});
};


// update user subscription
export const update = async (req, res) => {
	const customer = await Customer.findOne({ user_id: req.user._id });

	if (!customer.stripe_subscription_id) {
		return res.status(400).json({message: 'Customer is not subscribed to any plan.'});
	}

	const stripe_subscription = await stripe.subscriptions.update(customer.stripe_subscription_id, { plan: req.body.plan });
	const local_subscription  = await customer.set({ stripe_subscription_id: stripe_subscription.id, subscription_plan: stripe_subscription.plan.id, subscription_period_end: stripe_subscription.current_period_end }).save();
	res.status(200).json({message: 'Subscription Updated.'});
};

// unscribe user from subscription
export const destroy = async (req, res) => {
	const customer = await Customer.findOne({ user_id: req.user._id });

	if (!customer.stripe_subscription_id) {
		return res.status(400).json({message: 'Customer is not subscribed to any plan.'});
	}

	const stripe_subscription = await stripe.subscriptions.del(customer.stripe_subscription_id);
	const local_subscription  = await customer.set({ stripe_subscription_id: null, subscription_plan: null, subscription_period_end: null }).save();
	res.status(200).json({message: 'User unsubcribed successfully.'});
};


// Listen to webhooks for following events:

// 1: Listen to invoice.created hook and respond positively. Otherwise, stripe will take upto 72 hrs to charge payment.
// 2: Payment failure on recurring subscription. Notify user when this hook arrives.
// 3: Listen to invoice.payment_succeeded hook (on recurring subscription). Update 'subscription_period_end' field in DB on it's arrival, otherwise customer will lose access to dashboard.
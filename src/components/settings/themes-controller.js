// THEME CONTROLLER
// =============================================================================

// Import Modules
import mongoose from 'mongoose';
import fs from 'fs-extra';

// Import Models
import Setting from './setting-model';

// Import Middlewares
import * as tmw from './settings-middleware';


// Define Action(s)
export const index = async (req, res) => {
	const themes = res.locals.themes;

	// Mark active theme in `themes` array
	const setting = await Setting.findOne({ user_id: req.user._id });
	const current_theme = themes.find(theme => theme.name === tmw.toTitleCase(setting.current_theme));
	current_theme.active = "true";

	return res.status(200).json(themes);
};

export const read = async (req, res) => {
	const theme  = tmw.getThemeBy('name', req.params.name, res.locals.themes);

	if (theme.length !== 0) {
		return res.status(200).json(theme);
	} else {
		return res.status(400).json({message: 'No such theme exists.'});
	}
};

export const update = async (req, res) => {
	const themes = res.locals.themes;
	const theme  = tmw.getThemeBy('name', req.body.name, themes);

	if (theme.length === 0) {
		return res.status(400).json({message: 'No such theme exists.'});
	}

	// Update 'Setting' document
	const options = { new: true, runValidators: true };
	const setting = await Setting.findOneAndUpdate({ user_id: req.user._id }, { current_theme: req.body.name }, options).exec();

	// Update `themes` array with new active theme
	const current_theme = themes.find(theme => theme.name === tmw.toTitleCase(setting.current_theme));
	current_theme.active = "true";

	return res.status(200).json(themes);
};

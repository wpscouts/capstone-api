// SETTING MODEL
// =============================================================================

// Import Modules
import mongoose from 'mongoose';
import validator from 'validator';
import bcrypt from 'bcrypt';

// Fix Deprecated Warning
mongoose.Promise = global.Promise;


// Define Schema
const SettingSchema = new mongoose.Schema({
	user_id: {
		type: mongoose.Schema.ObjectId,
		ref: 'User',
		required: 'You must assign some user to this settings.'
	},
	site_title: {
		type: String,
		trim: true,
		required: 'The setting site title field is required.'
	},
	site_desc: {
		type: String,
		trim: true
	},
	site_subdomain: {
		type: String,
		trim: true,
		required: 'The site subdomain field is required.'
	},
	custom_url: {
		type: String,
		trim: true,
		validate: [validator.isURL, 'The custom url field must be a valid url.'],
	},
	current_theme: {
		type: String,
		trim: true,
		default: 'default',
		required: 'The current theme field is required.'
	},
	password: String,
});

// Database "Mutations" Layer: http://mongoosejs.com/docs/middleware.html
SettingSchema.pre('save', async function(next) {
	if (this.password) {
		const hash = await bcrypt.hash(this.password, 12);
		this.password = hash;
	}
	next();
});


// Export Schema
const SettingModel = mongoose.model('Setting', SettingSchema);
export default SettingModel;
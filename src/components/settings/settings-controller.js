// SETTING CONTROLLER
// =============================================================================

// Import Modules
import mongoose from 'mongoose';

// Import Models
import Setting from '../settings/setting-model';


// Define Action(s)
export const read = async (req, res) => {
	const setting = await Setting.findOne({ user_id: req.user._id });
	res.status(200).send(setting);
};

export const update = async (req, res) => {
	const options = { new: true, runValidators: true };
	const setting = await Setting.findOneAndUpdate({ user_id: req.user._id }, req.body, options).exec();
	res.status(200).send(setting);
};

// The 'Settings' document for each user is 'created' and 'deleted'
// with the same operation on the 'User' resource.
// SETTINGS MIDDLEWARE (Router Level)
// =============================================================================

// Import Modules
import fs from 'fs-extra';
import slug from 'slugs';

// Import Models
import Setting from './setting-model';


// Define Middleware(s)
export const isvalidSubdomain = async (req, res, next) => {
	
	// Subdomain Validation
	const subdomain_blacklist = await fs.readJson('./src/data/subdomain_blacklist.json');
	const is_blacklisted = subdomain_blacklist.includes(req.body.site_subdomain);

	if (is_blacklisted) {
		return res.status(400).json({ message: 'The subdomain you provided is not available.' });
	}

	next(); // there were no errors!
};




export const getThemes = async (req, res, next) => {
	// Store theme directories in `themesDirs` array
	const themesRoot = './static/themes';
	const themesDirs = await fs.readdir(themesRoot);

	// Store each theme information in `themes` array
	const themes = [];
	for (let i in themesDirs) {
		themes[i] = await fs.readJson(themesRoot + '/'+ themesDirs[i] +'/package.json');
		themes[i].slug = themesDirs[i];
		themes[i].url = 'http://lvh.me:9000/public/themes/'+ themesDirs[i];
		themes[i].active = "false";
	}

	// Carry forward `themes` array in response
	res.locals.themes = themes;

	next(); // there were no errors!
};


export const getThemeBy = (attr, value, source) => {
	const condition = (item) =>  slug(item[attr]) == slug(value);
	return source.filter(condition);
}

export const toTitleCase = (str) => {
    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
}

// VALIDATE USER REGISTRATION MIDDLE-WARE (Router Level)
// =============================================================================

// Import Modules
import fs from 'fs-extra';
import bcrypt from 'bcrypt';

// Import Models
import User from './user-model';


// Define Middleware(s)
export const isvalidRegister = async (req, res, next) => {
	
	// Subdomain Validation
	const subdomain_blacklist = await fs.readJson('./src/data/subdomain_blacklist.json');
	const is_blacklisted = subdomain_blacklist.includes(req.body.site_subdomain);

	if (is_blacklisted) {
		return res.status(400).json({ message: 'The subdomain you provided is not available.' });
	}

	// Other Field(s) Validation
	req.sanitizeBody('name');
	req.checkBody('name', 'You must supply a name!').notEmpty();
	req.checkBody('email', 'That Email is not valid!').isEmail();
	req.sanitizeBody('email').normalizeEmail({
		remove_dots: false,
		remove_extension: false,
		gmail_remove_subaddress: false
	});
	req.checkBody('password', 'Password Cannot be Blank!').notEmpty();
	req.checkBody('password-confirmation', 'Confirmed Password cannot be blank!').notEmpty();
	req.checkBody('password-confirmation', 'Oops! Your passwords do not match').equals(req.body.password);

	const errors = req.validationErrors();

	if (errors) {
		return res.status(402).json({ message: 'Got validation errors.', errors: errors });
	}

	next(); // there were no errors!
};


export const checkOriginalPassword = async (req, res, next) => {

	// check username against our record
	const user = await User.findOne({ _id: req.user._id });

	// check password against our record
	bcrypt.compare(req.body['password-current'], user.password).then(function(isMatch) {
		if (!isMatch) {
			res.status(401).json({message: 'You current password is not correct.'});
			return;
		} else {
			next(); // there were no errors!
		}
	});

};

export const validateNewPassword = (req, res, next) => {
	const isPassword = req.body.password;

	if (!isPassword) {
		return next();
	}

	req.checkBody('password-current', 'Please provide current password.').notEmpty();
	req.checkBody('password', 'Password Cannot be Blank!').notEmpty();
	req.checkBody('password-confirmation', 'Confirmed Password cannot be blank!').notEmpty();
	req.checkBody('password-confirmation', 'Oops! Your passwords do not match').equals(req.body.password);

	const errors = req.validationErrors();
	if (errors) {
		res.status(400).json({ message: 'Got validation errors.', errors: errors });
		return;
	}
	next(); // there were no errors!
};
// USER MODEL
// =============================================================================

// Import Modules
import __ from 'lodash';
import mongoose from 'mongoose';
import validator from 'validator';
import bcrypt from 'bcrypt';

// Fix Deprecated Warning
mongoose.Promise = global.Promise;


// Define Schema
const UserSchema = new mongoose.Schema({
	name: {
		type: String,
		required: 'Please supply a name',
		trim: true
	},
	email: {
		type: String,
		unique: true,
		lowercase: true,
		trim: true,
		validate: [validator.isEmail, 'Invalid Email Address'],
		required: 'Please Supply an email address'
	},
	password: {
		type: String,
		required: true,
		minlength: 6
	}
});


// Database "Mutations" Layer: http://mongoosejs.com/docs/middleware.html
async function hashPassword(next) {
	const hash = await bcrypt.hash(this.password, 12);
	this.password = hash;
	next();
}
async function hashPasswordOnUpdate(next) {
	if (this._update.password) {
		const hash = await bcrypt.hash(this._update.password, 12);
		this._update.password = hash;
	}
	next();
}

UserSchema.pre('save', hashPassword);
UserSchema.pre('findOneAndUpdate', hashPasswordOnUpdate);


// Database "Getters" Layer: http://mongoosejs.com/docs/guide.html#methods
UserSchema.methods.toJSON = function () {
	const user = this;
	const userObject = user.toObject();

	return __.pick(userObject, ['_id', 'name', 'email']);
};


// Export Schema
const UserModel = mongoose.model('User', UserSchema);
export default UserModel;
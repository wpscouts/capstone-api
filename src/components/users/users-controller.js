// USER CONTROLLER
// =============================================================================

// Import Modules
import __ from 'lodash';
import mongoose from 'mongoose';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import stripePackage from 'stripe';

//  Set Stripe Key
const stripe = stripePackage('sk_test_dZlZk75Gbz1XszV24M98eSwe');

// Import Models
import User from '../users/user-model';
import Setting from '../settings/setting-model';
import Page from '../pages/page-model';
import Customer from '../customers/customer-model';
import Authorization from '../authorization/authorization-model';


// Define Action(s)
export const create = async (req, res, next) => {
	const existing_user = await User.findOne({ email: req.body.email });

	if (!existing_user) {
		const user 		 	= await (new User(req.body)).save();
		
		const customer 		= new Customer({ user_id: user }).save();
		const setting 	 	= new Setting({ user_id: user, site_title: req.body.name, site_subdomain: req.body.site_subdomain }).save();
		const authorization = new Authorization({ user_id: user }).save();

		const result 	 	= await Promise.all([customer, setting, authorization]); // execute all async then proceed

		return res.status(200).json({message:"User registered successfully.", user});
	}
	
	res.status(400).json({message: 'User with this email already exists.'});
};

export const read = async (req, res, next) => {
	const user 		= await User.findOne({ _id: req.user._id });
	res.status(200).json(user);
};

export const update = async (req, res) => {
	const options = { new: true, runValidators: true };
	const user = await User.findOneAndUpdate({ _id: req.user._id }, req.body, options).exec();
	res.status(200).json(user);
};

export const destroy = async (req, res) => {
	// delete customer's stripe data
	const existing_customer = await Customer.findOne({ user_id: req.user._id });
	if (existing_customer.stripe_customer_id) {
		const stripe_subscription = await stripe.subscriptions.del(existing_customer.stripe_subscription_id);
		const stripe_customer = await stripe.customers.del(existing_customer.stripe_customer_id);
	}

	// delete user's local data (in DB)
	const setting 	 	= Setting.findOneAndRemove({ user_id: req.user._id });
	const authorization = Authorization.findOneAndRemove({ user_id: req.user._id });
	const pages 	 	= Page.remove({ user_id: req.user._id });
	const customer 	 	= Customer.findOneAndRemove({ user_id: req.user._id });
	const result 	 	= await Promise.all([setting, authorization, pages, customer]); // execute all async then proceed
	
	// delete user's original account (in DB)
	const user = await User.findOneAndRemove({ _id: req.user._id });
	
	res.status(200).json({ message: "User and all relevant data successfully deleted!" });
};

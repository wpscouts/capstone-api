// PAGE CONTROLLER
// =============================================================================

// Import Modules
import mongoose from 'mongoose';

// Import Models
import Page from './page-model';


// Define Action(s)
export const index = async (req, res) => {
	const pages = await Page.find({ user_id: req.user._id });
	res.status(200).send(pages);
};

export const create = async (req, res) => {
	req.body.user_id = req.user._id;
	const page = await (new Page(req.body)).save();
	res.status(200).send(page);
};

export const read = async (req, res) => {
	const page = await Page.findOne({ slug: req.params.slug, user_id: req.user._id });
	res.status(200).send(page);
};

export const update = async (req, res) => {
	const options = { new: true, runValidators: true };
	const page = await Page.findOneAndUpdate({ slug: req.params.slug, user_id: req.user._id }, req.body, options).exec();
	res.status(200).send(page);
};

export const destroy = async (req, res) => {
	const page = await Page.findOneAndRemove({ slug: req.params.slug, user_id: req.user._id });
	res.status(200).send({ message: "Page successfully deleted!" });
};
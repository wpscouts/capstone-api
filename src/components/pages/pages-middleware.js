// PAGES MIDDLE-WARE (Router Level)
// =============================================================================

// Import Modules
import fs from 'fs-extra';
import slug from 'slugs';

// Import Models
import Authorization from '../authorization/authorization-model';
import Page from './page-model';


// Define Middleware(s)
export const isAuthorized = async (req, res, next) => {
	const authorization = await Authorization.findOne({ user_id: req.user._id });
	const pages_count 	= await Page.count({ user_id: req.user._id });
	const page_limit 	= authorization.page_limit;

	console.log('Current Pages:' + pages_count);
	console.log('Pages Limit:' + page_limit);

	// res.locals.themes = themes;

	if (pages_count >= page_limit) {
		return res.status(402).json({ message: 'You reached your limit of maximum number of pages.' });
	}

	next(); // there were no errors!
};
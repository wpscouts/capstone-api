// PAGE MODEL
// =============================================================================

// Import Modules
import mongoose from 'mongoose';
import slug from 'slugs';
import bcrypt from 'bcrypt';

// Fix Deprecated Warning
mongoose.Promise = global.Promise;


// Define Schema
const PageSchema = new mongoose.Schema({
	user_id: {
		type: mongoose.Schema.ObjectId,
		ref: 'User',
		required: 'You must assign some user to this page.'
	},
	title: {
		type: String,
		trim: true,
		required: 'Please enter a page title.'
	},
	slug: String,
	content: {
		type: String,
		trim: true
	},
	status: {
		type: String,
		enum: ['public', 'private', 'hidden'],
		required: 'Please set a page status.'
	},
	password: String
});

// Database "Mutations" Layer: http://mongoosejs.com/docs/middleware.html
async function makeSlug(next) {
	this.slug = slug(this.title);
	next();
}
async function makeSlugOnUpdate(next) {
	if (this._update.title) {
		this._update.slug = slug(this._update.title);
	}
	next();
}

async function hashPassword(next) {
	if (this.password) {
		const hash = await bcrypt.hash(this.password, 12);
		this.password = hash;
	}
	next();
}
async function hashPasswordOnUpdate(next) {
	if (this._update.password) {
		const hash = await bcrypt.hash(this._update.password, 12);
		this._update.password = hash;
	}
	next();
}

PageSchema.pre('save', makeSlug, hashPassword);
PageSchema.pre('findOneAndUpdate', makeSlugOnUpdate, hashPasswordOnUpdate);


// Export Schema
const PageModel = mongoose.model('Page', PageSchema);
export default PageModel;